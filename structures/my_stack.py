class Stack(object):
    def __init__(self, stack_size=0):
        self.max = stack_size
        self.lista = [0] * stack_size
        self.top = None

    def push(self, val):
        return False

    def print(self):
        return False

    def pop(self):
        return False


if __name__ == "__main__":
    stos = Stack(2)
    stos.print()
    stos.push(3)
    stos.push(4)
    stos.push(9)

    print("Starting Stack")
    stos.print()
    print("After deleting")
    stos.pop()
    stos.print()
