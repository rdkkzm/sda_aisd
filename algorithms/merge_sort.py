def merge_sort(lista):
    return lista


if __name__ == "__main__":
    li = [1, 9, 8, 5, 7, 3, 3, 2, 4]

    print("Lista przed sortowaniem: {}".format(li))
    result = merge_sort(li)
    print("Lista po sortowaniu: {}".format(result))

    assert result == [1, 2, 3, 3, 4, 5, 7, 8, 9], "Lista nie jest posortowana!"
